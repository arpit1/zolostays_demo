package com.arpit.zolostaysdemo.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomTextViewRegular extends AppCompatTextView {

    public CustomTextViewRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Medium.otf");
        this.setTypeface(face);
    }
}
